﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandModelController : MonoBehaviour {
    private Animation animationComponent;
    private string animationClipName = "Armature|hold_stick";
    private bool triggerDown = false;
    private float animationSpeed = 4;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // トリガー押しっぱなしならアニメーションを50%の所で一時停止
        if(triggerDown && animationComponent[animationClipName].normalizedTime >= 0.5f) {
            animationComponent[animationClipName].speed = 0;
        }
	}

    void Awake() {
        animationComponent = GetComponent<Animation>();
    }

    public void TriggerUp() {
        // アニメーション速度を戻して離す所まで再生
        triggerDown = false;
        animationComponent[animationClipName].speed = animationSpeed;
    }

    public void TriggerDown() {
        // アニメーション開始
        triggerDown = true;
        animationComponent[animationClipName].speed = animationSpeed;
        animationComponent[animationClipName].normalizedTime = 0;
        animationComponent.Play(animationClipName);
    }
}
