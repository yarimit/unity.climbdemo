﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private enum Status {
        None,
        Climing,
        Falling,
        Landed
    }

    private Vector3 initialPosition;
    private Status status;
    private float timeLandedRestore;

    private Rigidbody RigidbodyComponent {
        get {
            return gameObject.GetComponent<Rigidbody>();
        }
    }

    private Collider Collider {
        get {
            return gameObject.GetComponent<Collider>();
        }
    }

    private SteamVR_ControllerManager ControllerManager {
        get {
            return gameObject.GetComponent<SteamVR_ControllerManager>();
        }
    }

    private HandController RightHand {
        get {
            return ControllerManager.right.GetComponent<HandController>();
        }
    }

    private HandController LeftHand {
        get {
            return ControllerManager.left.GetComponent<HandController>();
        }
    }


    void Awake() {
        initialPosition = gameObject.transform.position;
        status = Status.None;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateCheckGrab();

        UpdateCheckLand();

        UpdateRestoreLand();
	}

    private void SetGravity(bool useGravity) {
        RigidbodyComponent.useGravity = useGravity;
    }

    private void ActivateCollider(bool enabled) {
        Collider.enabled = enabled;
    }

    private bool IsGrabbingAny() {
        return (LeftHand.IsGrabbing() || RightHand.IsGrabbing());
    }

    private void UpdateCheckGrab() {
        if(IsGrabbingAny()) {
            // なんか掴んでる状態
            SetGravity(false);
            Collider.enabled = false;
            gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
            status = Status.Climing;
        } else {
            if(status == Status.Climing) {
                //// 落下開始
                //SetGravity(true);
                //Collider.enabled = true;
                //status = Status.Falling;
                //Debug.Log(gameObject.name + ": Falling");
            }
        }
    }

    public void HandReleased(HandController hand, SteamVR_Controller.Device controller) {
        if(!IsGrabbingAny()) {
            // 何も掴んでいない状態になった
            RigidbodyComponent.velocity = controller.velocity * -2;
            //RigidbodyComponent.angularVelocity = controller.angularVelocity * -1;

            // 落下開始
            SetGravity(true);
            Collider.enabled = true;
            status = Status.Falling;
            Debug.Log(gameObject.name + ": Falling velocity:" + RigidbodyComponent.velocity);
        }
    }

    private void UpdateCheckLand() {
        // 落下中でなければ何もしない
        if(status != Status.Falling) {
            return;
        }

        /*
        if (gameObject.transform.position.y <= initialPosition.y) {
            // 地面に落ちた・・・
            SetGravity(false);
            gameObject.transform.position =
                new Vector3(gameObject.transform.position.x,
                            initialPosition.y,
                            gameObject.transform.position.z);

            status = Status.Landed;
            timeLandedRestore = 0f;
            Debug.Log(gameObject.name + ": Landed");
        }
        */
    }

    private void UpdateRestoreLand() {
        if(status != Status.Landed) {
            return;
        }

        timeLandedRestore += Time.deltaTime;
        if(timeLandedRestore > 3.0f) {
            // 復帰処理
            LeftHand.enabled = true;
            RightHand.enabled = true;

            status = Status.None;
            gameObject.transform.position = initialPosition;
            gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
            RigidbodyComponent.isKinematic = false;

            Debug.Log(gameObject.name + ": Restore");
        }
    }

    /*
    public void OnTriggerEnter(Collider other) {
        if(status != Status.Falling) {
            return;
        }

        Debug.Log(gameObject.name + ": Collid to " + other.tag);

        if (other.tag == "Ground") {
            SetGravity(false);
            gameObject.transform.position =
                new Vector3(gameObject.transform.position.x,
                            initialPosition.y,
                            gameObject.transform.position.z);

            status = Status.Landed;
            timeLandedRestore = 0f;
            Debug.Log(gameObject.name + ": Landed");
        }
    }*/

    public void OnCollisionEnter(Collision collision) {
        if (status != Status.Falling) {
            return;
        }
        Debug.Log(gameObject.name + ": Collid to " + collision.gameObject.tag);

        if (collision.gameObject.tag == "Ground") {
            // 着地時処理
            SetGravity(false);
            gameObject.transform.position =
                new Vector3(gameObject.transform.position.x,
                            initialPosition.y,
                            gameObject.transform.position.z);
            RigidbodyComponent.velocity = Vector3.zero;
            RigidbodyComponent.angularVelocity = Vector3.zero;
            RigidbodyComponent.isKinematic = true;

            // 復帰タイマー設定
            // 復帰するまでコントローラ操作不可
            LeftHand.enabled = false;
            RightHand.enabled = false;

            status = Status.Landed;
            timeLandedRestore = 0f;
            Debug.Log(gameObject.name + ": Landed");
        }
    }

}
