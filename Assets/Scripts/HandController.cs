﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour {
    private GameObject cameraRigObject;
    private PlayerController playerController;
    private HandModelController handModelController;

    private SteamVR_TrackedObject trackedObject;
    private SteamVR_Controller.Device Controller {
        get {
            return SteamVR_Controller.Input((int)trackedObject.index);
        }
    }

    private Vector3 grabbedPosition;
    private Vector3 grabbedCameraRigRelatePosition;
    private GameObject collidingObject;
    private GameObject grabbingObject;

    void Awake() {
        trackedObject = GetComponent<SteamVR_TrackedObject>();
        cameraRigObject = gameObject.transform.parent.gameObject;
        playerController = cameraRigObject.GetComponent<PlayerController>();

        handModelController = gameObject.transform.Find("Model/hand_skin").GetComponent<HandModelController>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Controller.GetHairTriggerDown()) {
            Debug.Log(gameObject.name + " Trigger down.");

            if (collidingObject) {
                GrabObject();
            }

            handModelController.TriggerDown();
        }

        if (Controller.GetHairTriggerUp()) {
            Debug.Log(gameObject.name + " Trigger up.");
            if (grabbingObject) {
                ReleaseObject();
            }

            handModelController.TriggerUp();
        }

        if(grabbingObject) {
            var diffHandPosition = grabbedPosition - gameObject.transform.position;
            //Debug.Log(gameObject.name + " GrabMove : diff=" + diffHandPosition);
            cameraRigObject.transform.position += diffHandPosition;
        }
    }

    public void SetCollidingObject(Collider col) {
        if (collidingObject || !col.GetComponent<Rigidbody>()) {
            return;
        }

        collidingObject = col.gameObject;

        var co = collidingObject.GetComponent<GrabbableObject>();
        if (co) {
            co.Collid();
        }

        Debug.Log("Colliding " + collidingObject.name);
    }

    public void OnTriggerEnter(Collider other) {
        //Debug.Log("OnTriggerEnter " + other.gameObject.name);
        SetCollidingObject(other);
    }

    public void OnTriggerStay(Collider other) {
        //Debug.Log("OnTriggerStay" + other.gameObject.name);
        SetCollidingObject(other);
    }

    public void OnTriggerExit(Collider other) {
        if (!collidingObject) {
            return;
        }

        var go = collidingObject.GetComponent<GrabbableObject>();
        if (go) {
            go.CollidExit();
        }

        collidingObject = null;
    }

    public void GrabObject() {
        grabbingObject = collidingObject;
        collidingObject = null;

        var go = grabbingObject.GetComponent<GrabbableObject>();
        if (go) {
            go.Grab();
        }

        Debug.Log("GrabObject " + grabbingObject.name);

        grabbedPosition = gameObject.transform.position;
        Debug.Log("GrabbedPosition - " + grabbedPosition);

        grabbedCameraRigRelatePosition = grabbedPosition - cameraRigObject.transform.position;
        Debug.Log("grabbedCameraRigRelatePosition - " + grabbedCameraRigRelatePosition);
    }

    private void ReleaseObject() {
        var go = grabbingObject.GetComponent<GrabbableObject>();
        if (go) {
            go.Release();
        }

        Debug.Log("ReleaseObject " + grabbingObject.name);
        grabbingObject = null;

        // 手が離れた事を通知
        playerController.HandReleased(this, Controller);
    }

    public bool IsGrabbing() {
        return grabbingObject != null;
    }

}
