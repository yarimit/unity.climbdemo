﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableObject : MonoBehaviour {
    enum GrabStatus {
        None,
        Colliding,
        Grabbing
    };

    private Color defaultColor;
    private GrabStatus grabStatus;

	// Use this for initialization
    void Awake() {
        grabStatus = GrabStatus.None;
//        defaultColor = gameObject.GetComponent<Renderer>().material.color;
//        Debug.Log(gameObject.name + ":Default Color - " + defaultColor);
    }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Collid() {
        if(grabStatus == GrabStatus.None) {
            grabStatus = GrabStatus.Colliding;
//            gameObject.GetComponent<Renderer>().material.color = new Color(1f, 0, 0);
        }
    }

    public void CollidExit() {
        if (grabStatus == GrabStatus.Colliding) {
            grabStatus = GrabStatus.None;
//            gameObject.GetComponent<Renderer>().material.color = defaultColor;
        }
    }

    public void Grab() {
        grabStatus = GrabStatus.Grabbing;
//        gameObject.GetComponent<Renderer>().material.color = new Color(0, 1f, 0);
    }

    public void Release() {
        grabStatus = GrabStatus.None;
//        gameObject.GetComponent<Renderer>().material.color = defaultColor;
    }

}
